import { Map } from './map.js'
import { BlockStack } from './blockStack.js'
import { Block } from './block.js'

export class Player {

    constructor(control, controlsString, canvas, context) {
        this.mapColor = '#282828'
        this.blockOpacity = 'ff'
        this.mapWidth = 10
        this.mapHeight = 10
        this.filling = 'fillOnce'

        this.canvas = canvas
        this.context = context
        this.controlsString = controlsString
        this.bStack = new BlockStack(this.blockOpacity)
        this.control = control
        this.pos = { x: 0, y: 0 }
        this.block = this.bStack.getNext()
        this.map = this.createMap()

        this.setListeners()
        this.drawBlock()
        console.log('Player ready')
    }

    smartFill() {
        this.reset()
        let dots = ">"
        while(this.autoMergeBestBlock()) {
            dots = dots.slice(0, -1)
            dots += '=>'
            console.log(dots)
        }
        console.log(`smartFilled up to ${this.map.getFillPercentage()}%`)
        console.log("DONE")
    }

    dumbFill() {
        this.reset()
        let dots = ">"
        while(this.autoMergeBlock()) {
            dots = dots.slice(0, -1)
            dots += '->'
            console.log(dots)
        }
        console.log(`dumbFilled up to ${this.map.getFillPercentage()}%`)
        console.log("DONE")
    }

    autoMergeBlock() {
        this.findBestPosition()

        let bp = this.map.bestPosition

        if (bp.record) {
            this.goToBestPosition(bp)
            this.map.mergeBlock(this)
            this.block = this.bStack.getNext(this.filling)
            if(!this.block) {
                return false
            }
            return true
        } else {
            console.log("There is no room for this piece.")
            return false
        }
    }
    

    autoMergeBestBlock() {
        let numberOfBlocks = this.bStack.getSizeOfStack()
        
        this.findBestPosition()

        for (let i = 0; i < numberOfBlocks; i++) {
            this.setToZeroCoordinates()
            this.nextBlock()
            this.findBestPosition()
        }
        let bp = this.map.bestPosition

        if (bp.record) {
            this.goToBestPosition(bp)
            this.map.mergeBlock(this)
            if(this.filling !== "fillWhatever") {
                this.block = this.bStack.getNext(this.filling)
                if(!this.block) {
                    return false
                }
            }
            return true
        } else {
            console.log("There is no room for this piece.")
            return false
        }
    }
    
    setListeners() {
        // MOVE listener
        document.addEventListener('keydown', e => {
            this.move(e)
        })

        // type of filling
        const fillOnce = document.getElementById("fillOnce")
        const fillAgain = document.getElementById("fillAgain")
        const fillWhatever = document.getElementById("fillWhatever")
        fillOnce.addEventListener('click', e => {
            this.filling = 'fillOnce'
        })
        fillAgain.addEventListener('click', e => {
            this.filling = 'fillAgain'
        })
        fillWhatever.addEventListener('click', e => {
            this.filling = 'fillWhatever'
        })


        // + - ... size of map
        const plusSize = document.getElementById("plusSize")
        const minusSize = document.getElementById("minusSize")
        plusSize.addEventListener('click', e => {
            this.enlargeMap()
        })
        minusSize.addEventListener('click', e => {
            this.reduceMap()
        })

        // AUTO-MERGE
        const dumbPlace = document.getElementById('dumbPlace')
        dumbPlace.addEventListener('click', e => {
            this.autoMergeBlock()
        })

        // AUTO-MERGE-BEST, AUTO-FILLS
        const smartPlace = document.getElementById('smartPlace')
        const smartFill = document.getElementById('smartFill')
        const dumbFill = document.getElementById('dumbFill')
        smartPlace.addEventListener('click', e => {
            this.autoMergeBestBlock()
        })
        smartFill.addEventListener('click', e => {
            this.smartFill()
        })
        dumbFill.addEventListener('click', e => {
            this.dumbFill()
        })

        const reset = document.getElementById('reset')
        reset.addEventListener('click', e => {
            this.reset()
        })

        const up = document.getElementById('up')
        const down = document.getElementById('down')
        const left = document.getElementById('left')
        const right = document.getElementById('right')
        const merge = document.getElementById('merge')
        up.addEventListener('click', e => {
            if (this.pos.y - 1 >= 0) {
                this.pos.y -= 1;
            }
            this.drawBlock()
        })
        down.addEventListener('click', e => {
            if (this.pos.y + this.block.height < this.map.height) {
                this.pos.y += 1;
            }
            this.drawBlock()
        })
        left.addEventListener('click', e => {
            if (this.pos.x - 1 >= 0) {
                this.pos.x -= 1;
            }
            this.drawBlock()
        })
        right.addEventListener('click', e => {
            if (this.pos.x + this.block.width < this.map.width) {
                this.pos.x += 1;
            }
            this.drawBlock()
        })
        merge.addEventListener('click', e => {
            this.mergeBlock()
            this.drawBlock()
        })

        const rotateLeft = document.getElementById('rotateLeft')
        const rotateRight = document.getElementById('rotateRight')
        const flip = document.getElementById('flip')
        rotateLeft.addEventListener('click', e => {
            this.rotate(0)
        })
        rotateRight.addEventListener('click', e => {
            this.rotate(1)
        })
        flip.addEventListener('click', e => {
            this.flip()
        })

        const nextBlock = document.getElementById('nextBlock')
        const previousBlock = document.getElementById('previousBlock')
        nextBlock.addEventListener('click', e => {
            this.nextBlock()
        })
        previousBlock.addEventListener('click', e => {
            this.prevBlock()
        })
    }


    move(e) {

        switch (e.keyCode) {
            case this.control.left:
                if (this.pos.x - 1 >= 0) {
                    this.pos.x -= 1;
                }
                this.drawBlock()
                break;
            case this.control.right:
                if (this.pos.x + this.block.width < this.map.width) {
                    this.pos.x += 1;
                }
                this.drawBlock()
                break;
            case this.control.up:
                if (this.pos.y - 1 >= 0) {
                    this.pos.y -= 1;
                }
                this.drawBlock()
                break;
            case this.control.down:
                if (this.pos.y + this.block.height < this.map.height) {
                    this.pos.y += 1;
                }
                this.drawBlock()
                break;
            case this.control.rotateLeft: this.rotate(0); break;
            case this.control.rotateRight: this.rotate(1); break;
            case this.control.flip: this.flip(); break;
            case this.control.mergeBlock: this.mergeBlock(); this.drawBlock(); break;
            case this.control.autoMergeBlock: this.autoMergeBlock(); break;
            case this.control.autoMergeBestBlock: this.autoMergeBestBlock(); break;
            case this.control.autoFill: this.autoFill(); break;
            case this.control.prevBlock: this.prevBlock(); break;
            case this.control.nextBlock: this.nextBlock(); break;
            case this.control.reset: this.reset(); break;
        }
    }

    colideArena() {
        this.block.matrix
        this.map.mapMatrix

        for (let y = 0; y < this.block.matrix.length; ++y) {
            for (let x = 0; x < this.block.matrix[y]; ++x) {
                if (this.block.matrix[y][x] !== 0 && (this.map.mapMatrix[y + this.pos.y] && this.map.mapMatrix[y + this.pos.y][x + this.pos.x] !== 0)) {
                    return true
                }
            }
        }
    }


    createMap() {
        this.setMapSizeLabels()
        return new Map(this.mapWidth, this.mapHeight, this.mapColor, 42, this.canvas, this.context)
    }

    setMapSizeLabels() {
        const widthLabel = document.getElementById("widthLabel")
        const heightLabel = document.getElementById("heightLabel")

        widthLabel.textContent = this.mapWidth
        heightLabel.textContent = this.mapHeight
    }

    enlargeMap() {
        this.setMapSizeLabels(++this.mapWidth, ++this.mapHeight)
        this.reset()
    }

    reduceMap() {
        this.setMapSizeLabels(--this.mapWidth, --this.mapHeight)
        this.reset()
    }

    mergeBlock() {
        this.map.mergeBlock(this)
        this.block = this.bStack.getNext()
    }

    mergeBestBlock() {
        let bp = this.map.bestPosition
        if (bp.record) {
            this.goToBestPosition(bp)
        }
        this.map.mergeBlock(this)
        this.block = this.bStack.getNext()
    }


    goToBestPosition(bp) {
        if (this.block.color !== bp.block.color) {
            this.bStack.addBlock(new Block(this.block.color, this.block.matrix))
            this.block = this.bStack.removeBlockByColor(bp.block.color)
            this.block.matrix = bp.block.matrix
            this.pos = bp.pos
        } else {
            this.block.matrix = bp.block.matrix
            this.pos = bp.pos
        }
        this.drawBlock()
    }

    setToZeroCoordinates() {
        this.pos = { x: 0, y: 0 }
    }

    prevBlock() {
        this.block = this.bStack.prevBlock(this.block)
        this.drawBlock()
    }

    nextBlock() {
        this.block = this.bStack.nextBlock(this.block)
        this.drawBlock()
    }


    drawBlock() {
        this.map.drawBlock(this)
    }

    rotate(dir) {
        this.block.rotate(dir)
        this.drawBlock()
    }

    flip() {
        this.block.flip()
        this.drawBlock()
    }

    findBestPosition() {

        for (let flip = 0; flip < 2; flip++) {
            this.setToZeroCoordinates()
            this.flip()
            for (let rotation = 0; rotation < 4; rotation++) {
                this.setToZeroCoordinates()
                this.rotate(1)
                this.findBestPositionInRotation()
            }
        }
    }

    deepClone(obj) {
        return JSON.parse(JSON.stringify(obj))
    }


    findBestPositionInRotation() {

        const xTimes = this.map.width - this.block.width + 1
        const yTimes = this.map.height - this.block.height + 1

        for (let row = 0; row < yTimes; row++) {
            this.pos.y = row
            this.pos.x = 0

            for (let col = 0; col < xTimes; col++) {
                this.pos.x = col
                this.map.drawBlock(this)
            }
        }
    }


    reset() {
        //console.log(this.controlsString)
        this.map = this.createMap()
        this.bStack = new BlockStack(this.blockOpacity)
        this.pos = { x: 0, y: 0 }
        this.block = this.bStack.getNext()
        this.drawBlock()
    }
}