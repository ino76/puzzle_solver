export class Block {

    constructor(color, matrix) {
        this.color = color
        this.matrix = matrix
        this.width = this.matrix[0].length
        this.height = this.matrix.length

        this.volume = this.width * this.height
        this.mass = this.getMass()
        this.complexity = this.volume / this.mass
    }

    getMass() {

        let number = 0

        this.matrix.forEach((row, y) => {
            row.forEach((value, x) => {
                if(value !== 0) {
                    number++
                }
            })
        })

        return number
    }

    rotate(dir) {
        let matrixW = this.matrix[0].length
        let matrixH = this.matrix.length

        let newMatrix = []
        for(let i = 0; i < matrixW; i++) {
            newMatrix.push([])

            for(let j = 0; j < matrixH; j++) {
                if(dir) {
                    newMatrix[i].unshift(this.matrix[j][i])
                } else {
                    newMatrix[i].push(this.matrix[j][matrixW - i - 1])
                }
            }
        }

        this.setMatrix(newMatrix)
    }

    flip() {
        let newMatrix = []
        
        this.matrix.forEach((row, i) => {
            newMatrix.push([])
            row.forEach(value => newMatrix[i].unshift(value))
        })

        this.matrix = newMatrix
    }

    setMatrix(matrix) {
        this.matrix = matrix
        this.width = matrix[0].length
        this.height = matrix.length
    }

}