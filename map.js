import { Block } from "./block.js"

export class Map {

    constructor(width, height, BgColor, BOX_SIZE, canvas, context) {
        this.BOX_SIZE = BOX_SIZE
        this.BgColor = BgColor
        this.width = width
        this.height = height
        this.canvas = canvas
        this.context = context

        this.bestPosition = {
            record: 0,
            matrix: null
        }

        this.mapMatrix = this.createMatrix(width, height)

        canvas.width = this.width * BOX_SIZE
        canvas.height = this.height * BOX_SIZE
        context.scale(BOX_SIZE, BOX_SIZE)

        this.drawMap()
        this.updateFillPercentage()
    }

    getFillPercentage() {
        const all = this.width * this.height
        let filled = 0
        this.mapMatrix.forEach(row => filled += row.filter(v => v !== 0).length)
        return Math.floor(filled / all * 100)
    }

    drawMap() {
        this.context.fillStyle = this.BgColor
        this.context.fillRect(0, 0, this.width, this.height)

        this.mapMatrix.forEach((row, y) => {
            row.forEach((value, x) => {
                if (value !== 0) {
                    this.context.fillStyle = value
                    this.context.fillRect(x, y, 1, 1)
                }
            })
        })
    }

    createMatrix(width, height) {
        const matrix = []
        while (height--) {
            matrix.push(new Array(width).fill(0))
        }

        return matrix
    }


    // draw a block, return OBJECT
    // 
    //  colide: Boolean
    //  record: Number
    //
    drawBlock({ block, pos }) {
        this.drawMap()
        //console.log(JSON.stringify(pos))
        let touches = 0
        let colides = 0

        block.matrix.forEach((row, y) => {
            row.forEach((value, x) => {
                if (value !== 0) {
                    let touch = this.checkTouch(x + pos.x, y + pos.y)

                    this.context.fillStyle = "#ffffff14"
                    this.context.fillRect(x + pos.x, y + pos.y, 1, 1)

                    if (touch > 0 && touch < 5) {
                        this.context.fillStyle = '#ffffff25'
                        this.context.fillRect(x + pos.x, y + pos.y, 1, 1)
                        touches += touch
                    }

                    if (touch === 5) {
                        this.context.fillStyle = '#ff000075'
                        this.context.fillRect(x + pos.x, y + pos.y, 1, 1)
                        colides++
                    }
                }
            })
        })

        let record = touches / block.mass
        if (touches && !colides) {
            if (this.bestPosition.record < record) {
                this.bestPosition = {
                    record,
                    block: new Block(block.color, block.matrix),
                    pos: Object.assign({}, pos),
                    colides,
                    touches
                }
            }
        }
    }

    resetBestPosition() {
        this.bestPosition = {
            record: 0,
            block: null,
            pos: null
        }
    }

    checkTouch(x, y) {
        let touch = 0
        // if colides
        if (this.mapMatrix[y][x] !== 0) {
            return 5
        }

        if (this.mapMatrix[y][x - 1] !== 0) {
            touch += 1
        }
        if ( this.mapMatrix[y][x + 1] !== 0)  {
            touch += 1
        }
        if ((y - 1 < 0 || this.mapMatrix[y - 1][x] !== 0) || this.mapMatrix[y - 1][x] === undefined) {
            touch += 1
        }
        if ((y + 1 >= this.mapMatrix.length || this.mapMatrix[y + 1][x] !== 0)) {
            touch += 1
        }
        return touch
    }


    mergeBlock({ block, pos }) {

        block.matrix.forEach((row, y) => {
            row.forEach((value, x) => {
                if (value !== 0) {
                    this.mapMatrix[y + pos.y][x + pos.x] = block.color
                }
            })
        })

        this.resetBestPosition()
        this.updateFillPercentage()
    }

    updateFillPercentage() {
        let percentageValue = this.getFillPercentage()
        let percentageElement = document.getElementById('filled')
        percentageElement.textContent = percentageValue
    }
}