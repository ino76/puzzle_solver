import { Block } from "./block.js"

export class BlockStack {

    constructor(opacity) {
        this.opacity = opacity
        this.blocks = this.fill()

        this.blocks = this.blocks.sort((a, b) => a.complexity > b.complexity ? 1 : -1)
    }

    getNext(filling) {
        let next = this.blocks.pop()
        if (next === undefined) {
            if(filling === "fillOnce") {
                return undefined
            }
            this.blocks = this.fill()
            return this.getNext()
        } else {
            return next
        }
    }

    getSizeOfStack() {
        return this.blocks.length
    }

    removeBlockByColor(color) {
        let removedBlock;
        this.blocks = this.blocks.filter(b => {
            if (b.color === color) {
                removedBlock = b
            }
            return b.color !== color
        })
        return removedBlock
    }

    addBlock(block) {
        this.blocks.push(block)
    }

    prevBlock(playersBlock) {
        this.blocks.push(playersBlock)
        return this.blocks.shift()
    }

    nextBlock(playersBlock) {
        this.blocks.unshift(playersBlock)
        return this.blocks.pop()
    }


    fill() {
        const blocks = [
            // ORIGINAL  123e63 - 337AA6 - 05C7F2 - F2D750 - F2A74B
            // {
            //     color: '#123e63' + this.opacity,
            //     matrix: [
            //         [1, 0, 0],
            //         [1, 0, 0],
            //         [1, 1, 1],
            //         [1, 1, 1],
            //         [0, 1, 0]]
            // },
            // {
            //     color: '#337AA6' + this.opacity,
            //     matrix: [
            //         [0, 1, 1, 1, 1],
            //         [1, 1, 1, 1, 1],
            //         [0, 1, 1, 1, 1],
            //         [0, 1, 1, 1, 1],
            //         [0, 0, 0, 1, 1]]
            // },
            // {
            //     color: '#05C7F2' + this.opacity,
            //     matrix: [
            //         [0, 1, 1],
            //         [0, 1, 1],
            //         [0, 1, 1],
            //         [1, 1, 1],
            //         [1, 1, 1],
            //         [1, 1, 1]
            //     ]
            // },
            // {
            //     color: '#F2D750' + this.opacity,
            //     matrix: [
            //         [1, 1, 0, 1, 1],
            //         [1, 1, 1, 1, 1],
            //         [1, 1, 1, 1, 1],
            //         [1, 1, 0, 0, 0]]
            // },
            // {
            //     color: '#F2A74B' + this.opacity,
            //     matrix: [
            //         [0, 1, 0, 0, 0, 0],
            //         [0, 1, 0, 0, 0, 0],
            //         [0, 1, 0, 0, 0, 0],
            //         [1, 1, 1, 1, 1, 1]]
            // },

            // TETRIS  123e63 - 337AA6 - 05C7F2 - F2D750 - F2A74B
            // {
            //     color: '#123e63' + this.opacity,
            //     matrix: [
            //         [1, 1,],
            //         [1, 1,]]
            // },
            // {
            //     color: '#337AA6' + this.opacity,
            //     matrix: [
            //         [1],
            //         [1],
            //         [1],
            //         [1]]
            // },
            // {
            //     color: '#05C7F2' + this.opacity,
            //     matrix: [
            //         [1, 1, 0],
            //         [0, 1, 1,]]
            // },
            // {
            //     color: '#F2D750' + this.opacity,
            //     matrix: [
            //         [1, 0, 0],
            //         [1, 1, 1]]
            // },
            // {
            //     color: '#F2A74B' + this.opacity,
            //     matrix: [
            //         [0, 1, 0],
            //         [1, 1, 1]]
            // },

            // Pentomino  123e63 - 337AA6 - 05C7F2 - F2D750 - F2A74B
            {
                color: '#123e63' + this.opacity,
                matrix: [
                    [1],
                    [1],
                    [1],
                    [1],
                    [1],
                ]
            },
            {
                color: '#337AA6' + this.opacity,
                matrix: [
                    [1,1,0],
                    [0,1,1],
                    [0,1,0]
                ]
            },
            {
                color: '#05C7F2' + this.opacity,
                matrix: [
                    [1,0],
                    [1,0],
                    [1,0],
                    [1,1,],
                ]
            },
            {
                color: '#F2D750' + this.opacity,
                matrix: [
                    [1,1],
                    [1,1],
                    [1,0]]
            },
            {
                color: '#F2A74B' + this.opacity,
                matrix: [
                    [1,0],
                    [1,0],
                    [1,1],
                    [0,1],
                ]
            },
            {
                color: '#22bde0' + this.opacity,
                matrix: [
                    [1,1,1],
                    [0,1,0],
                    [0,1,0],
                ]
            },
            {
                color: '#2278e0' + this.opacity,
                matrix: [
                    [1,0,1],
                    [1,1,1]
                ]
            },
            {
                color: '#6e22e0' + this.opacity,
                matrix: [
                    [0,0,1],
                    [0,0,1],
                    [1,1,1],
                ]
            },
            {
                color: '#6e22e0' + this.opacity,
                matrix: [
                    [1,0,0],
                    [1,1,0],
                    [0,1,1],
                ]
            },
            {
                color: '#e04222' + this.opacity,
                matrix: [
                    [0,1,0],
                    [1,1,1],
                    [0,1,0],
                ]
            },
            {
                color: '#dd22e0' + this.opacity,
                matrix: [
                    [0,1],
                    [1,1],
                    [0,1],
                    [0,1],
                ]
            },
            {
                color: '#22e0ad' + this.opacity,
                matrix: [
                    [1,1,0],
                    [0,1,0],
                    [0,1,1],
                ]
            }
        ]

        return blocks.map(b => new Block(b.color, b.matrix))
    }
}