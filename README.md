# Puzzle solver

    =========================================
                    Controls
    =========================================
        up:                   W
        down:                 S
        left:                 A
        right:                D

        rotateLeft:           Left
        rotateRight:          Right
        flip:                 Shift
        
        mergeBlock:           Space
        prevBlock:            Q
        nextBlock:            E

        auto. mergeBlock:     F
        auto. mergeBestBlock: C

        reset:                Enter

Online k vyzkouseni [zde](http://dlx.surge.sh/).


Po precteni zadaneho ukolu jsem si uvedomil, ze je algoritmicky pomerne slozity. Rozhodne slozitejsi,
nez co jsem zvykly resit, coz me docela prekvapilo vzhledem k tomu ze se uchazim o pozici frontendaka.

Nicmene udelal jsem si mensi pruzkum a zjistil jsem ze na podobne problemy je velmi uzitecny tzv. 
Algoritmus X ve spojeni s Dancing links (dlx) jak je popsal Donald Knuth. (Pozn.: On ovsem resi 'exact cover' problem
coz toto zadani neni).

Cim dele jsem se do problemu noril, tim vice jsem nabyval presvedceni, ze pro nejoptimalnejsi algoritmus,
ktery by na jedne strane vybiral tu nejidealnejsi moznou pozici bloku a zaroven chytre sporil zbytecne
kroky nejsem dostatecne vzdelany v matematice a zbehly v algoritmech. Mel jsem tedy chut to vzdat.

Protoze jsem se, ale prave v algoritmech a pote v navrhovych vzorech chtel dovzdelat, rekl jsem si ze 
to zkusim. Nebyl uz ale cas se dovzdelavat a tak jsem zkusil zda bych nedokazal prijit na nejake reseni
ciste ze sve hlavy, bez pouziti jakychkoliv cizich zdroju. 

Pro tento ucel jsem si vytvoril nastroj, ktery zobrazoval dany blok, umel ho rotovat nebo prevratit a
zjistoval, ktere jeho casti sousedi s jinymi 'pevnymi' bloky nebo okrajem mapy.

Premyslel jsem nad nekolika moznostmi jak urcit zda je pozice bloku nejlepsi z hlediska celkoveho usporadani
a napadlo me ze pomerne idealni je stav kdy ma blok co nejvyssi pomer  [pevnych bloku matice / bloku sousedicich s bloky nebo okraji]

Moje reseni je pomerne neusporne pokud jde o narocnost vypoctu. Kontroluji proste vsechny mozne pozice tzn.
Ctyri rotace bloku pokazde projdou vsechny pozice v mape a hlidaji si dany [koeficient dotyku].
Blok potom prevratim a kontroluji znovu ctyri rotace. Vim ze pro nektere bloky by se daly rotace usetrit,
ale mym jedinym cilem bylo dokazat najit alespon castecne funkcni algoritus a teprve az pak se venovat pripadne
optimalizaci.

Muzu rict ze algoritmus pomerne funguje (zatim poklada jen prave vybranou kostku, ale rozsirit ho na prochazeni vsech kostek, pripadne i na kompletni polozeni zasobniku uz nebude takovy problem).

Jeste jsem trochu experimentoval s nalezenim idealniho poradi kostek tak, aby jejich defaultni poradi bylo co nejidealnejsi.
K tomu jsem si vymyslel 'slozitost' kostky ktera byla  dana jednoduse jen jako [vsechna pole matice / plna pole matice] a zkousel jsem
jak bude algoritmus uspesny pokud bloky seradim podle slozitosti tak ci onak, ale nevidel jsem zasadni rozdil. Nechal jsem je 
serazene od nejmene slozitych (matice ma prevazne plne bloky) po ty vice slozite.

Jako sadu bloku jsem si nastavil zatim presne ty stejne jako byly v zadani a protoze docasne je nastavil tak
ze po odebrani posledniho bloku se zasobnik znovu naplni.
Zamerne jsem jeste upravil chovani tak, aby se pri AUTOMATICKEM MERGOVANI matice nezobrazila barva bloku - je
tak lepe zretelne, kde se matice dotyka okoli.